# NoSQL DataBase

## Abstract

There are multiple type of database availbale there and we can choose different database according to our need and use. In this paper I have done some study over 5 different NoSQL databases.

***

## Introduction 

NoSQL stands for "not only SQL". It doesn't store data in tables like SQL databases does. It stores data in documnets, key-value pairs etc. These are Highly scalable and reliable.

***

## Types

These are follwing types of NoSQL databases.

* Document-Based NoSQL Databases
* Key-Value Databases
* Wide Column-Based Databases
* Graph Bases Databases

***

Following are the use cases of 5 NoSQL databases.

### 1. MongoDB

* If you have a numerous data sources.
* A lots of read and write operation to perfomed

Ex. Uber, eBay

***

### 2. DyanamoDB

* When very high rate of read and write operations is needed.
* If simple key-value queries are needed.
* If high consitency is required for ticket bookings or banking system.

Ex. Snapchat, Samsung

***

### 3. Cassandra

* If your system needs more write operations than read operations.
* Data consistency is not a prior requirement.

Ex. Netflix, Coursera, Instagram

***

### 4. ElasticSearch

* ElasticSearch is used when you have a text search.
* If you want to make quires from database on the basis of a text.

Ex. Stackoverflow, Medium, Udemy

***

### 5. HBase

* This is best suited when you a large amount of data has to be processed.
* It can be used for storing real time messages for billion of users.
* If want to have real-time access to the data.

Ex. Hike, Pintrest

***

## Refereces
* https://www.analyticsvidhya.com/blog/2020/09/different-nosql-databases-every-data-scientist-must-know/
